Портал работы с ПИФ

Domain representation
![Domain representation](docs/AP.png)


User scenarios

1. Пользователь оформляет заявку на приобритение паев фонда А
2. Validation service проверяет корректность пользовательской заявки
3. Корректная заявка передается в:
   a)  Print Form Service для формирования печатной формы
   b) ClientAccount service для внесение изменений об приобретении паев фонда А.
   c) XML/Request/CSV Export services для выгрузки в форматах, пригодных для взаимодействия с внешними АС
    